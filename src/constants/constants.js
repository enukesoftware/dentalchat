export default Constants = {
  //url: "https://dc2blog.dentalchat.com/server/",
  //url: "https://dev1blog.dentalchat.com/server/"
  url: "https://blog.dentalchat.com/server/api/",
  // url: "https://awsapi.dentalchat.com/server/api/",
  // url: "https://dc2blog.dentalchat.com/server/",

  imageUrl: "https://blog.dentalchat.com/server/",
  ios: 3.11,
  android: 40.0,
  baseColor: "#1272F6",
  googleKey: "AIzaSyCECNx6YKAjaYfP9Eq7FXAMB1QmjUKvMZk",
  API_URL:{
    getArchivedPost:"get-archived-post",
    archivePost:"archive-post",
    followUp:"service/dentistservice/follow-up",
    addMyPatient:"add-my-patient",
    removeMyPatient:"remove-my-patient",
    getLostMessages:"get-lost-messages",
    videoConsultationRequest:'request-video-consulation',
    createVideoMeeting:'create-video-meeting',
    meetingRoomLicenseRequest:'meeting-room-license-request',
	}
};
