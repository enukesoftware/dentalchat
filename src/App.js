/* @flow */

import React, { Component } from "react";
import { Text, TextInput, Alert } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer,
  createSwitchNavigator
} from "react-navigation";
import Home from "./screens/Home";
import DentistLogin from "./screens/DentistLogin";
import DentistDashboard from "./screens/DentistDashboard";
import DentistMessages from "./screens/DentistMessages";
import DentistAccount from "./screens/DentistAccount";
import DentistChatWindow from "./screens/DentistChatWindow";
import PostDetails from "./screens/PostDetails";
import DentistEditProfile from "./screens/DentistEditProfile";
import DentistChangePassword from "./screens/DentistChangePassword";
import DentistForgotPassword from "./screens/DentistForgotPassword";
import PatientRegistration from "./screens/PatientRegistration";
import PatientForgetPassword from "./screens/PatientForgetPassword";
import PatientAccount from "./screens/PatientAccount";
import PatientProfileUpdate from "./screens/PatientProfileUpdate";
import PatientLogin from "./screens/PatientLogin";
import PatientDashBoard from "./screens/PatientDashBoard";
import PatientMessages from "./screens/PatientMessages";
import PatientChatWindow from "./screens/PatientChatWindow";
import PatientPostDetails from "./screens/PatientPostDetails";
import PatientDoctorList from "./screens/PatientDoctorList";
import PatientCreatePost from "./screens/PatientCreatePost";
import PatientDoctorProfile from "./screens/PatientDoctorProfile";
import PatientPostDetailsFromList from "./screens/PatientPostDetailsFromList";
import PatientChangePassword from "./screens/PatientChangePassword";
import DentistRegistration from "./screens/DentistRegistration";
import DentistProfileUpdate from "./screens/Profile";
import Ionicons from "react-native-vector-icons/Ionicons";
import IconBadge from "react-native-icon-badge";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import mainHome from "./screens/mainHome";
import constants from "./constants/constants";
import DentistPostDetails from "./screens/DentistPostDetails";
import subscripitonScreen from "./screens/subscripitonScreen";
import subscriptionPurchase from "./screens/subscriptionplanPurchase";
import dentistSubscription from "./screens/dentistSubscription";
import dentistProfileUpdate2 from "./screens/Profile-2";
import dentistProfileUpdate3 from "./screens/Profile-3";
import dentistProfileUpdate4 from "./screens/Profile-4";
import dentistProfileUpdate5 from "./screens/Profile-5";
import dentistProfileUpdate6 from "./screens/Profile-6";
import dentistProfileUpdate7 from "./screens/Profile-7";
import DisplayImage from "./screens/DisplayImage";
import SearchLanguage from "./screens/SearchLanguage";
import editProfile from "./screens/editProfile";
import ImagePicker from "react-native-image-picker";
import MyClientList from './screens/MyClientList';

//#############PATIENT

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;

const PatientAccountStack = createStackNavigator(
  {
    PatientAccount: {
      screen: PatientAccount
    },
    PatientChangePassword: {
      screen: PatientChangePassword
    },
    PatientProfileUpdate: {
      screen: PatientProfileUpdate
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false
    }
  }
);
const PatientDashBoardStack = createStackNavigator(
  {
    PatientDashBoard: {
      screen: PatientDashBoard
    },
    PatientDoctorList: {
      screen: PatientDoctorList
    },
    PatientDoctorProfile: {
      screen: PatientDoctorProfile
    },
    PatientCreatePost: {
      screen: PatientCreatePost
    },
    PatientPostDetailsFromList: {
      screen: PatientPostDetailsFromList
    },
    PatientMessages: {
      screen: PatientMessages
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false
    }
  }
);
const PatientCreatePostStack = createStackNavigator(
  {
    mainHome: {
      screen: mainHome
    },
    PatientCreatePost: {
      screen: PatientCreatePost
    }
  },
  {
    defaultNavigationOptions: {
      headerTitleAllowFontScaling: false
    }
  }
);
const PatientMessagesStack = createStackNavigator(
  {
    PatientMessages: {
      screen: PatientMessages
    },
    PatientProfileUpdate: {
      screen: PatientProfileUpdate
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false
    }
  }
);
const PatientMainTab = createBottomTabNavigator(
  {
    Consult: {
      screen: PatientCreatePostStack,
      navigationOptions: ({ navigation }) => ({
        title: "Home",
        headerTintColor: "white",
        tabBarIcon: ({ focused, tintColor }) => {
          return (
            <Ionicons
              name="ios-disc"
              type="ionicon"
              color={tintColor}
              size={30}
            />
          );
        }
      })
    },
    Message: {
      screen: PatientMessagesStack,
      navigationOptions: ({ navigation }) => ({
        title: "Message",
        headerTintColor: "white",
        tabBarIcon: ({ focused, tintColor }) => {
          return (
            <IconBadge
              MainElement={
                <Ionicons
                  name="ios-mail"
                  type="ionicon"
                  color={tintColor}
                  size={27}
                />
              }
              BadgeElement={
                global.MyVar && global.MyVar > 0
                  ? <Text style={{ color: "white", fontSize: 12 }}>
                    {global.MyVar}
                  </Text>
                  : <></>
              }
              IconBadgeStyle={{
                width: global.MyVar && global.MyVar > 0 ? 18 : 0,
                top: -2,
                right: -6,
                height: global.MyVar && global.MyVar > 0 ? 18 : 0,
                backgroundColor: "#FF0000"
              }}
            />
          );
        }
      })
    },
    Dashboard: {
      screen: PatientDashBoardStack,
      navigationOptions: ({ navigation }) => ({
        title: "Dashboard",
        headerTintColor: "white",
        tabBarIcon: ({ focused, tintColor }) => {
          return (
            <Ionicons
              name="ios-list-box"
              type="ionicon"
              color={tintColor}
              size={30}
            />
          );
        }
      })
    },
    More: {
      screen: PatientAccountStack,
      navigationOptions: ({ navigation }) => ({
        title: "More",
        headerTintColor: "white",
        allowFontScaling: "false",
        tabBarIcon: ({ focused, tintColor }) => {
          return (
            <Ionicons
              name="ios-more"
              type="ionicon"
              color={tintColor}
              size={30}
            />
          );
        }
      })
    }
  },
  {
    tabBarOptions: {
      allowFontScaling: false,
      showIcon: true,
      inactiveTintColor: "black",
      activeTintColor: constants.baseColor
    }
  }
);
PatientMainTab.navigationOptions = {
  // Hide the header from AppNavigator stack
  header: null,
  title: "ddffds"
};
const PatientStack = createStackNavigator(
  {
    PatientMainTab: PatientMainTab,
    PatientChatWindow: PatientChatWindow,
    PatientPostDetails: PatientPostDetails,
    PatientDoctorProfile: PatientDoctorProfile
  },
  {
    initialRouteName: "PatientMainTab",
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: "#ffffff"
      },
      headerTitleAllowFontScaling: false,

      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);
//###########DENTIST
const DentistMessageStackNav = createStackNavigator(
  {
    DentistMessages: {
      screen: DentistMessages
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false
    }
  }
);

const DentistDashboardStackNav = createStackNavigator(
  {
    DentistDashboard: {
      screen: DentistDashboard
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false
    }
  }
);

const DentistAccountStackNav = createStackNavigator(
  {
    DentistAccount: {
      screen: DentistAccount
    },
    editProfile: {
      screen: editProfile
    },
    DentistEditProfile: {
      screen: DentistEditProfile
    },
    DentistProfileUpdate: {
      screen: DentistProfileUpdate
    },
    DentistProfileUpdate2: {
      screen: dentistProfileUpdate2
    },
    DentistProfileUpdate3: {
      screen: dentistProfileUpdate3
    },
    DentistProfileUpdate4: {
      screen: dentistProfileUpdate4
    },
    DentistProfileUpdate5: {
      screen: dentistProfileUpdate5
    },
    DentistProfileUpdate6: {
      screen: dentistProfileUpdate6
    },
    DentistProfileUpdate7: {
      screen: dentistProfileUpdate7
    },
    DentistSubscription: {
      screen: dentistSubscription
    },
    DentistChangePassword: {
      screen: DentistChangePassword
    },
    DisplayImage: {
      screen: DisplayImage
    },
    SearchLanguage: {
      screen: SearchLanguage
    },
    MyClientList:{
      screen:MyClientList
    }
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false
    }
  }
);
const DentistTabMenu = createBottomTabNavigator(
  {
    // Dashboard: {
    //     screen: DentistDashboardStackNav,
    //     navigationOptions: ({navigation}) => ({
    //       title:'Dashboard',
    //         headerTintColor: 'white',
    //         tabBarIcon: ({focused, tintColor}) => {
    //           return (
    //             <Ionicons
    //                 name='ios-list-box'
    //                 type='ionicon'
    //                 color={tintColor}
    //                 size={30}
    //             />
    //         );
    //         },
    //     })
    // },
    Message: {
      screen: DentistMessageStackNav,
      tabBarOptions: { allowFontScaling: false },
      navigationOptions: ({ navigation }) => ({
        title: "Message",
        headerTintColor: "white",
        tabBarIcon: ({ focused, tintColor }) => {
          return (
            <IconBadge
              MainElement={
                <Ionicons
                  name="ios-mail"
                  type="ionicon"
                  color={tintColor}
                  size={30}
                />
              }
              BadgeElement={
                global.MyDental && global.MyDental > 0
                  ? <Text style={{ color: "white", fontSize: 12 }}>
                    {global.MyDental}
                  </Text> : <></>
              }
              IconBadgeStyle={{
                width: global.MyDental && global.MyDental > 0 ? 18 : 0,
                top: -2,
                right: -6,
                height: global.MyDental && global.MyDental > 0 ? 18 : 0,
                backgroundColor: "#FF0000"
              }}
            />
          );
        }
      })
    },
    Account: {
      tabBarOptions: { allowFontScaling: false },
      screen: DentistAccountStackNav,
      navigationOptions: ({ navigation }) => ({
        title: "More",
        headerTintColor: "white",
        tabBarIcon: ({ focused, tintColor }) => {
          return (
            <Ionicons
              name="ios-more"
              type="ionicon"
              color={tintColor}
              size={30}
            />
          );
        }
      })
    }
  },
  {
    tabBarOptions: {
      allowFontScaling: false,
      showIcon: true,
      inactiveTintColor: "black",
      activeTintColor: constants.baseColor
    }
  }
);
DentistTabMenu.navigationOptions = {
  // Hide the header from AppNavigator stack
  header: null,
  title: "ddffds"
};
const LoginStack = createStackNavigator(
  {
    Home: Home,
    DentistLogin: DentistLogin,
    DentistForgotPassword: DentistForgotPassword,
    DentistRegistration: DentistRegistration,
    PatientLogin: PatientLogin,
    PatientForgetPassword: PatientForgetPassword,
    PatientRegistration: PatientRegistration
  },
  {
    defaultNavigationOptions: {
      headerTitleAllowFontScaling: false
    }
  }
);
const DentistStack = createStackNavigator(
  {
    subscripitonScreen: subscripitonScreen,
    subscriptionPurchase: subscriptionPurchase,
    DentistTabMenu: DentistTabMenu,
    PatientPostDetails: PatientPostDetails,
    DentistChatWindow: DentistChatWindow,
    DentistPostDetails: DentistPostDetails
  },
  {
    initialRouteName: "subscripitonScreen",
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerTitleAllowFontScaling: false,
      headerStyle: {
        backgroundColor: "#ffffff"
      },
      //headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);
//############COMMON###########
const SimpleApp = createSwitchNavigator(
  {
    // DentistTabMenu: DentistTabMenu,
    AuthLoading: LoginStack,
    App: DentistStack,
    Auth: PatientStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);
const container = createAppContainer(SimpleApp);
export default container;
